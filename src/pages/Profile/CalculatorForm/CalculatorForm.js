import React from 'react';
import classNames from 'classnames';
import styles from '../Form.module.css';
import ErrorWindow from '../../../shared/ErrorWindow';

function CalculatorForm({ userData, setUserData, update }) {
  const [errMessage, setErrMessage] = React.useState('');
  const [weightPref, setWeightPref] = React.useState('leave');
  const [weight, setWeight] = React.useState('');
  const [height, setHeight] = React.useState('');
  const [totalCalories, setCalories] = React.useState('');

  const handleSave = async () => {
    if (totalCalories === '') {
      setErrMessage('You miss the data!');
      return false;
    }
    if (isNaN(Number(totalCalories))) {
      setErrMessage('Incorrect type of data!');
      return false;
    }
    if (Number(totalCalories) < 300) {
      setErrMessage('Data is not valid!');
      return false;
    }
    setUserData({ ...userData, calories: `${totalCalories}` });
    userData.calories = `${totalCalories}`;
    console.log(userData);
    update(userData);
  };

  const handleCalc = async () => {
    let prefCal = 0;

    if (weight === '' || height === '') {
      setErrMessage('You miss the data!');
      return false;
    }
    if (isNaN(Number(weight)) || isNaN(Number(height))) {
      setErrMessage('Incorrect type of data!');
      return false;
    }
    if (Number(weight) < 5 || Number(height) < 30) {
      setErrMessage('Data is not valid!');
      return false;
    }
    switch (weightPref) {
      case 'lose':
        prefCal = -500;
        break;
      case 'gain':
        prefCal = 500;
        break;
    }
    switch (userData.sex) {
      case 'male':
        setCalories(Math.round(66.5 + 13.75 * weight + 5.003 * height - 6.775 * 20 + prefCal));
        break;
      case 'female':
        setCalories(Math.round(655.1 + 9.563 * weight + 1.85 * height - 4.676 * 20 + prefCal));
        break;
    }
  };

  return (
    <div className={styles.base_form}>
      <h1>Calories calculator</h1>
      <br />
      <div className={styles.container}>
        <input
          className={classNames(styles.base_input, styles.paired)}
          placeholder="Your height, cm"
          value={height}
          onChange={(event) => setHeight(event.target.value)}
        />
        <input
          className={classNames(styles.base_input, styles.paired)}
          placeholder="Your weight, kg"
          value={weight}
          onChange={(event) => setWeight(event.target.value)}
        />
      </div>

      <div className={styles.container}>
        <input
          type="radio"
          id="lose"
          name="weight_pref"
          value="lose"
          checked={weightPref === 'lose'}
          onChange={(event) => setWeightPref(event.target.value)}
        />
        <label htmlFor="lose" className={styles.radio_label}>lose</label>

        <input
          type="radio"
          id="leave"
          name="weight"
          value="leave"
          checked={weightPref === 'leave'}
          onChange={(event) => setWeightPref(event.target.value)}
        />
        <label htmlFor="leave" className={styles.radio_label}>leave</label>

        <input
          type="radio"
          id="gain"
          name="weight"
          value="gain"
          checked={weightPref === 'gain'}
          onChange={(event) => setWeightPref(event.target.value)}
        />
        <label htmlFor="gain" className={styles.radio_label}>gain</label>

      </div>
      <button type="button" className={styles.base_button} onClick={handleCalc}>Count calories</button>
      <br />
      <label className={styles.input_label}>
        If you already know your norm, just put the number(current:
        {userData.calories}
        ):
      </label>
      <input
        className={classNames(styles.base_input, styles.small)}
        placeholder="Number of kilocalories"
        value={totalCalories}
        onChange={(event) => setCalories(event.target.value)}
      />
      <button type="button" className={styles.base_button} onClick={handleSave}>Save</button>
      <ErrorWindow errMessage={errMessage} setErrMessage={setErrMessage}>
        {errMessage}
      </ErrorWindow>

    </div>
  );
}

export default CalculatorForm;
