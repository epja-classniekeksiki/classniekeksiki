import React from 'react';
import styles from '../Form.module.css';
import ErrorWindow from '../../../shared/ErrorWindow';

function PersonForm({ userData, setUserData, update }) {
  const [errMessage, setErrMessage] = React.useState('');

  const handleSave = async () => {
    if (userData.name < 2) {
      setErrMessage('Name is not valid!');
      return false;
    }
    update(userData);
  };

  return (
    <div className={styles.base_form}>
      <h1>Personal data</h1>
      <br />
      <label className={styles.input_label}>Enter your new name:</label>
      <input
        className={styles.base_input}
        placeholder="Your name"
        value={userData.name}
        onChange={(event) => setUserData({ ...userData, name: event.target.value })}
      />
      <div className={styles.container}>
        <input
          type="radio"
          id="male"
          name="sex"
          value="male"
          checked={userData.sex === 'male'}
          onChange={(event) => setUserData({ ...userData, sex: event.target.value })}
        />
        <label htmlFor="male" className={styles.radio_label}>Male</label>

        <input
          type="radio"
          id="female"
          name="sex"
          value="female"
          checked={userData.sex === 'female'}
          onChange={(event) => setUserData({ ...userData, sex: event.target.value })}
        />
        <label htmlFor="female" className={styles.radio_label}>Female</label>
      </div>

      <button type="button" className={styles.base_button} onClick={handleSave}>Save</button>
      <ErrorWindow errMessage={errMessage} setErrMessage={setErrMessage}>
        {errMessage}
      </ErrorWindow>

    </div>
  );
}

export default PersonForm;
