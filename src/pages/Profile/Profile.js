import React, {
  useState, useEffect, useContext,
} from 'react';
import {
  Link, Route, Routes, useNavigate,
} from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';
import styles from './Profile.module.css';
import PersonForm from './PersonForm/PersonForm';
import { URLs } from '../../URLs';
import { AuthContext } from '../../context';
import ChangePasswordForm from './ChangePasswordForm/ChangePasswordForm';
import CalculatorForm from './CalculatorForm/CalculatorForm';
import SuccessWindow from '../../shared/SuccessWindow';
import {getUserData} from "../../api";

function Profile(props) {
  const API = getConfigValue('classniekeksiki.api');
  const { setUsername } = useContext(AuthContext);
  const navigate = useNavigate();
  const [userData, setUserData] = useState({
    name: '',
    alias: '',
    password: '',
    calories: '',
    sex: '',

  });
  const [success, setSuccess] = useState('');
  useEffect(() => { getUserData(localStorage.getItem('username')).then(data => setUserData(data)) }, []);



  const updateUserData = async (updatedData) => {
    fetch(`${API}/update`, {
      method: 'PUT',
      body: JSON.stringify(updatedData),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((response) => {
        setSuccess('user data is updated!');
        if (response.ok) {
          setSuccess('user data is updated!');
          return true;
        }
        return false;
      });
  };

  return (
    <div className={styles.profile_page}>
      <nav className={styles.left_nav}>
        <ul className={styles.ul_}>
          <Link to={URLs.profile.url}><li>Personal data</li></Link>
          <Link to={`${URLs.profile.url}/auth`}><li>Change password</li></Link>
          <Link to={`${URLs.profile.url}/calc`}><li>Set calories</li></Link>
          <br />
          <li onClick={() => {
            localStorage.removeItem('username');
            setUsername(null);
            navigate(URLs.recipes.url);
          }}
          >
            Logout
          </li>
        </ul>
      </nav>
      <div className={styles.right_container}>
        <Routes>
          <Route path="/" element={<PersonForm userData={userData} setUserData={setUserData} update={updateUserData} />} />
          <Route path="/auth" element={<ChangePasswordForm userData={userData} setUserData={setUserData} update={updateUserData} />} />
          <Route path="/calc" element={<CalculatorForm userData={userData} setUserData={setUserData} update={updateUserData} />} />
        </Routes>
      </div>
      <SuccessWindow message={success} setMessage={setSuccess}>
        {success}
      </SuccessWindow>
    </div>
  );
}

export default Profile;
