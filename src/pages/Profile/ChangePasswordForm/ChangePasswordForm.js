import React from 'react';
import classNames from 'classnames';
import styles from '../Form.module.css';
import ErrorWindow from '../../../shared/ErrorWindow';

function ChangePasswordForm({ userData, setUserData, update }) {
  const [errMessage, setErrMessage] = React.useState('');
  const [curPassword, setCurPassword] = React.useState('');
  const [newPassword, setNewPassword] = React.useState('');
  const [repeatPassword, setRepeatPassword] = React.useState('');

  const handleSave = async () => {
    if (userData.password !== curPassword) {
      setErrMessage('Current password is incorrect!');
      return false;
    }
    if (newPassword < 2) {
      setErrMessage('New password is not valid!');
      return false;
    }
    if (newPassword === curPassword) {
      setErrMessage('Password is already used!');
      return false;
    }
    if (newPassword !== repeatPassword) {
      setErrMessage('Passwords doesn\'t match!');
      return false;
    }
    setUserData({ ...userData, password: newPassword });
    userData.password = newPassword;
    update(userData);
  };

  return (
    <div className={styles.base_form}>
      <h1>Auth data</h1>
      <br />

      <input
        className={classNames(styles.base_input, styles.blocked)}
        placeholder="Your login"
        value={userData.alias}
        readOnly
      />
      <br />
      <label className={styles.input_label}>Enter your current password:</label>
      <input
        type="password"
        className={styles.base_input}
        placeholder="Current password"
        value={curPassword}
        onChange={(event) => setCurPassword(event.target.value)}
      />
      <label className={styles.input_label}>Create new password:</label>
      <input
        type="password"
        className={styles.base_input}
        placeholder="New password"
        value={newPassword}
        onChange={(event) => setNewPassword(event.target.value)}
      />
      <input
        type="password"
        className={styles.base_input}
        placeholder="Repeat password"
        value={repeatPassword}
        onChange={(event) => setRepeatPassword(event.target.value)}
      />

      <button type="button" className={styles.base_button} onClick={handleSave}>Save</button>

      <ErrorWindow errMessage={errMessage} setErrMessage={setErrMessage}>
        {errMessage}
      </ErrorWindow>
    </div>
  );
}

export default ChangePasswordForm;
