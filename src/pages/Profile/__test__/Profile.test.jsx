import renderer from 'react-test-renderer';
import React from 'react';
import PersonForm from "../PersonForm/PersonForm";
import CalculatorForm from "../CalculatorForm/CalculatorForm";
import ChangePasswordForm from "../ChangePasswordForm/ChangePasswordForm";
const userData={
  "name":"aaa",
  "alias":"test",
  "password":"test",
  "calories":"2000",
  "sex":"male"
}
const setUserData=()=>{}
const update=()=>{}
describe('Profile component', () => {
  describe('Person form render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<PersonForm userData={userData} setUserData={setUserData} update={update}/>);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
  describe('Calculator render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<CalculatorForm userData={userData} setUserData={setUserData} update={update}/>);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
  describe('Change password form render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<ChangePasswordForm userData={userData} setUserData={setUserData} update={update}/>);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
