import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getRecipeByID } from '../../api/edamam';
import Ingredients from './components/Ingredients/Ingredients.jsx';
import Tags from './components/Tags/Tags.jsx';
import Nutrition from './components/Nutrition/Nutrition.jsx';

import {
  RecipeInfoWrapper,
  RecipeName,
  RecipePicture,
  RecipeDetails,
  RecipeContent,
} from './RecipeInfo.styled.jsx';

function RecipeInfo() {
  const { id } = useParams();

  const [recipeData, setRecipeData] = useState(false);

  useEffect(async () => {
    const response = await getRecipeByID(id);

    const formatedData = {
      image: (response.images.LARGE) ? response.images.LARGE.url : response.images.REGULAR.url,
      ingredients: response.ingredients,
      label: response.label,
      tags:
        (response.healthLabels.length > 5
          ? response.healthLabels.slice(0, 5)
          : response.healthLabels),
      nutritions: {
        carbohydrates: Math.floor(response.totalNutrients.CHOCDF.quantity),
        fat: Math.floor(response.totalNutrients.FAT.quantity),
        protein: Math.floor(response.totalNutrients.PROCNT.quantity),
        calories: Math.floor(response.totalNutrients.ENERC_KCAL.quantity),
      },
    };

    setRecipeData(formatedData);
  }, []);

  if (!recipeData) {
    return <h1>Loading</h1>;
  }

  return (
    <RecipeInfoWrapper>
      <RecipeName>{recipeData.label}</RecipeName>

      <RecipeContent>
        <RecipePicture src={recipeData.image} />
        <RecipeDetails>
          <Ingredients ingredients={recipeData.ingredients} />
          <Tags tags={recipeData.tags} />
          <Nutrition nutrition={recipeData.nutritions} />
        </RecipeDetails>
      </RecipeContent>
    </RecipeInfoWrapper>
  );
}

export default RecipeInfo;
