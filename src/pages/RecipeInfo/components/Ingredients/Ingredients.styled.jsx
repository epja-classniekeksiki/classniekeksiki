import styled from 'styled-components';

export const Wrapper = styled.div`
margin-bottom: 20px; 
`;

export const Headline = styled.div`
`;

export const Ingredient = styled.div`
  margin: 0 auto;
  width: 80%;
  display: grid;
  grid-template-columns: 8fr 1fr 3fr;
  grid-auto-rows: 20px;
`;
