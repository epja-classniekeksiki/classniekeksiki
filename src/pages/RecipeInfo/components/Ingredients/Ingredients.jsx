import React from 'react';
import { Wrapper, Headline, Ingredient } from './Ingredients.styled.jsx';

function Ingredients({ ingredients }) {
  return (
    <Wrapper>
      <Headline>
        {ingredients.length}
        {' '}
        ingredients
      </Headline>
      {ingredients.map((ingr) => (
        <Ingredient>
          <span>
            {ingr.food}
          </span>
          <span>
            {Math.ceil(ingr.quantity)}
          </span>
          <span>
            {((ingr.measure) ? ingr.measure : '\t')}
          </span>
        </Ingredient>
      ))}
    </Wrapper>
  );
}

export default Ingredients;
