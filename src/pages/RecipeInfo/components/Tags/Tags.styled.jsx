import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 25px;
  width: 455px;
  margin-bottom: 45px;
`;

export const Tag = styled.div`
  height: 28px;
  background-color: white;
  border-radius: 15px;
  padding: 5px 10px;
  user-select: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font: 300 14px "Roboto", sans-serif;
  transition: all 0.3s;
  box-shadow: 1px 2px 4px 0 #aaaaaa;
  width: 130px;
`;
