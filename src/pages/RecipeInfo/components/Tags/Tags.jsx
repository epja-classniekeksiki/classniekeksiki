import React from 'react';
import { Wrapper, Tag } from './Tags.styled.jsx';

function Tags({ tags }) {
  return (
    <Wrapper>
      {tags.map((tag) => <Tag>{tag}</Tag>)}
    </Wrapper>
  );
}

export default Tags;
