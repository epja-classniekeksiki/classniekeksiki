import React from 'react';

import { Wrapper, Nutritions, Calories } from './Nutrition.styled.jsx';

function Nutrition({ nutrition }) {
  return (
    <Wrapper>
      <Nutritions>
        <span>
          {`${nutrition.carbohydrates} `}
          {' '}
          carbons
        </span>
        <span>
          {`${nutrition.fat} `}
          fat
        </span>
        <span>
          {`${nutrition.protein} `}
          {' '}
          protein
        </span>

      </Nutritions>
      <Calories>
        {`${nutrition.calories} `}
        {' '}
        kCalories
      </Calories>
    </Wrapper>
  );
}
export default Nutrition;
