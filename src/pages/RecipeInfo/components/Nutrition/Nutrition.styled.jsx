import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: center;
`;
export const Nutritions = styled.div`
  display: inline-flex;
  justify-content: space-between;
  gap: 50px;
`;
export const Calories = styled.div`
    font-weight: 700;
`;
