import styled from 'styled-components';

export const RecipeInfoWrapper = styled.div`
  width: 1520px;
  margin: 0 auto;
  font-family: Roboto, serif;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const RecipeName = styled.div`
  display: inline-block;
  margin-bottom: 24px;
  margin-top: 44px;
  max-width: 900px;
  font-size: 48px;
  font-weight: 700;
`;

export const RecipeDetails = styled.div`
`;

export const RecipePicture = styled.img`
  display: block;
  border-radius: 15px;
  height: 500px;
`;

export const RecipeContent = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  gap: 50px;
  justify-content: center;
`;

export const Nutrition = styled.div``;
