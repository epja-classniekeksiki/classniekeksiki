import renderer from 'react-test-renderer';
import React from 'react';

import RegistrationForm from '../RegistrationForm';

describe('Login component', () => {
  describe('when there are registration form', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<RegistrationForm />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
