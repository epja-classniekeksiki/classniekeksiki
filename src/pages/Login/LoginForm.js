import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';
import styles from './Login.module.css';
import { URLs } from '../../URLs';
import ErrorWindow from '../../shared/ErrorWindow';

import { AuthContext } from '../../context';

function LoginForm({ onRegister }) {
  const API = getConfigValue('classniekeksiki.api');
  const { setUsername } = useContext(AuthContext);
  const navigate = useNavigate();

  const [loginData, setLoginData] = useState({
    alias: '', password: '',
  });

  const [errMessage, setErrMessage] = React.useState('');

  const handleLogin = async () => {
    if (loginData.alias < 2) {
      setErrMessage('Alias is not valid!');
      return false;
    }
    if (loginData.password < 2) {
      setErrMessage('Password is not valid!');
      return false;
    }
    fetch(`${API}/login`, {
      method: 'POST',
      body: JSON.stringify(loginData),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((response) => {
        if (response.ok) {
          const personalData = response.json();
          personalData.then((data) => {
            localStorage.setItem('username', data.alias);
            setUsername(data.alias);
          });
          navigate(URLs.diary.url);
          return true;
        }
        setErrMessage(`${response.status} user not found`);
        return false;
      });
  };

  return (
    <div className={styles.auth_form}>
      <h1>Authorization</h1>
      <input
        className={styles.auth_input}
        placeholder="Your nickname"
        value={loginData.alias}
        onChange={(event) => setLoginData({ ...loginData, alias: event.target.value })}
      />
      <input
        type="password"
        className={styles.auth_input}
        placeholder="Your password"
        value={loginData.password}
        onChange={(event) => setLoginData({ ...loginData, password: event.target.value })}
      />
      <div className={styles.buttons_container}>
        <button type="button" className={styles.auth_button} id={styles.login_btn} onClick={handleLogin}>Login</button>
        <button type="button" className={styles.auth_button} id={styles.reg_btn} onClick={onRegister}>Registration</button>
      </div>
      <ErrorWindow errMessage={errMessage} setErrMessage={setErrMessage}>
        {errMessage}
      </ErrorWindow>

    </div>
  );
}

export default LoginForm;
