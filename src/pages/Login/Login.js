import React, { useState } from 'react';
import LoginImage from '../../../static/login_image.jpg';
import styles from './Login.module.css';
import LoginForm from './LoginForm';
import RegistrationForm from './RegistrationForm';

function Login() {
  const [authType, setAuthType] = useState('login');
  return (
    <div className={styles.auth_page}>
      <img src={LoginImage} className={styles.auth_left} alt="Tasty dish" />
      <div className={styles.auth_right}>
        {authType === 'login'
          ? <LoginForm onRegister={() => setAuthType('registration')} />
          : <RegistrationForm onLogin={() => setAuthType('login')} />}
      </div>
    </div>
  );
}

export default Login;
