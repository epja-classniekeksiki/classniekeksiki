import React, { useState } from 'react';
import { getConfigValue } from '@ijl/cli';
import styles from './Login.module.css';
import ErrorWindow from '../../shared/ErrorWindow';

const API = getConfigValue('classniekeksiki.api');

function RegistrationForm({ onLogin }) {
  const [registrationData, setRegistrationData] = useState({
    name: 'test',
    alias: '',
    password: '',
    calories: '2000',
    sex: 'male',
  });
  const [repeatedPassword, setRepeatedPassword] = useState('');

  const [errMessage, setErrMessage] = React.useState('');

  const handleSignup = async () => {
    if (registrationData.alias < 2) {
      setErrMessage('Alias is not valid!');
      return false;
    }
    if (registrationData.password < 2) {
      setErrMessage('Password is not valid!');
      return false;
    }
    if (registrationData.password !== repeatedPassword) {
      setErrMessage('Passwords are not match!');
      return false;
    }
    fetch(`${API}/signup`, {
      method: 'POST',
      body: JSON.stringify(registrationData),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((response) => {
        if (response.ok) {
          onLogin();
          return true;
        }
        setErrMessage('User already exists!');
        return false;
      });
  };

  return (
    <div className={styles.auth_form}>
      <h1>Registration</h1>
      <input
        type="text"
        className={styles.auth_input}
        placeholder="Create nickname"
        autoComplete="off"
        value={registrationData.alias}
        onChange={(event) => setRegistrationData({ ...registrationData, alias: event.target.value })}
      />
      <input
        type="password"
        className={styles.auth_input}
        placeholder="Create password"
        autoComplete="off"
        value={registrationData.password}
        onChange={(event) => setRegistrationData({ ...registrationData, password: event.target.value })}
      />
      <input
        type="password"
        className={styles.auth_input}
        placeholder="Repeat password"
        autoComplete="off"
        value={repeatedPassword}
        onChange={(event) => setRepeatedPassword(event.target.value)}
      />
      <button type="button" className={styles.auth_button} id={styles.registration_btn} onClick={handleSignup}>
        Create new account
      </button>

      <ErrorWindow errMessage={errMessage} setErrMessage={setErrMessage}>
        {errMessage}
      </ErrorWindow>

    </div>
  );
}

export default RegistrationForm;
