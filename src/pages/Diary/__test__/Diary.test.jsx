import renderer from 'react-test-renderer';
import React from 'react';
import Diary from '../Diary.jsx';

jest.mock('mobx-react', () => ({
  observer: (el) => el,
}));

jest.mock('../store/store.js', () => ({
  mealsfetchingStatus: 'notFetched',
  dialyCalorieIntake: 2000,
  activeMeal: undefined,
  breakfast: [],
  lunch: [],
  dinner: [],
  snack: [],
  totalFats: 0,
  totalCarbs: 0,
  totalProteins: 0,
  totalCalories: 0,
  fetchMeals: jest.fn(),
}));

describe('Diary component', () => {
  describe('when meals are not fetched', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<Diary />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
