import React from 'react';
import { observer } from 'mobx-react';
import DiaryWrapperStyled, { DiaryLayoutStyled } from './Diary.styled';
import { H1 } from '../../shared/typography';
import DiarySummaryBlock from './components/DiarySummaryBlock/DiarySummaryBlock.jsx';
import DayScheduleBlock from './components/DayScheduleBlock/DayScheduleBlock.jsx';
import store from './store/store';

const Diary = observer(() => {
  React.useEffect(() => {
    if (store.mealsfetchingStatus === 'notFetched') {
      store.fetchMeals();
    }
  }, [store.mealsfetchingStatus]);

  if (store.mealsfetchingStatus === 'fetched') {
    return (
      <DiaryWrapperStyled>
        <H1>Main page</H1>
        <DiaryLayoutStyled>
          <DayScheduleBlock />
          <DiarySummaryBlock />
        </DiaryLayoutStyled>
      </DiaryWrapperStyled>
    );
  }
  return <H1> Loading... </H1>;
});

export default Diary;
