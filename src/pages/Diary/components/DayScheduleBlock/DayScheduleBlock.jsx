import { observer } from 'mobx-react';
import React from 'react';
import BreakfastIcon from '../../../../shared/icons/BreakfastIcon.jsx';
import DinnerIcon from '../../../../shared/icons/DinnerIcon.jsx';
import LunchIcon from '../../../../shared/icons/LunchIcon.jsx';
import SnackIcon from '../../../../shared/icons/SnackIcon.jsx';
import store from '../../store/store.js';
import Meal from './components/Meal/Meal.jsx';

const DayScheduleBlock = observer(() => (
  <div>
    <Meal
      name="Breakfast"
      products={store.breakfast}
    >
      <BreakfastIcon />
    </Meal>
    <Meal name="Lunch" products={store.lunch}>
      <LunchIcon />
    </Meal>
    <Meal name="Dinner" products={store.dinner}>
      <DinnerIcon />
    </Meal>
    <Meal name="Snack" products={store.snack}>
      <SnackIcon />
    </Meal>
  </div>
));

export default DayScheduleBlock;
