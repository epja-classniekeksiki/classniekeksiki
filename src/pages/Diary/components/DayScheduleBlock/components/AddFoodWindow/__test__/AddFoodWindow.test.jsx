import renderer from 'react-test-renderer';
import React from 'react';
import AddFoodWindow from '../index';

describe('AddFoodWindow component', () => {
  describe('AddFoodWindow render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<AddFoodWindow />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
