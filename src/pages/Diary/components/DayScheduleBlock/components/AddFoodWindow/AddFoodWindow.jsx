import React, { useState } from 'react';
import styles from './AddFoodWindow.module.css';

import SearchPanel from '../../../../../Recipes/SearchPanel';
import ModalWindow from '../../../../../../shared/ModalWindow';
import DishList from '../DishList/DishList.jsx';

function AddFoodWindow({ active, setActive, mealName }) {
  const [recipesList, setRecipesList] = useState([]);
  return (
    <ModalWindow active={active} setActive={() => { setActive(!active); }}>
      <SearchPanel onSubmit={setRecipesList} />
      <button type="button" className={styles.close_btn} onClick={() => { setActive(!active); }}>
        <span>&times;</span>
      </button>
      <DishList dishList={recipesList} mealName={mealName}/>
    </ModalWindow>
  );
}

export default AddFoodWindow;
