import styled from 'styled-components';

const MealStyled = styled.div`
  width: 100%;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  padding: 15px;
`;

export const MealHeaderStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const MealTitleStyled = styled.div`
  display: flex;
  column-gap: 15px;
  align-items: center;
`;

export const MealNameStyled = styled.h1`
  font-family: 'Roboto',serif;
  font-style: normal;
  font-weight: 100;
  font-size: 36px;
  line-height: 42px;
  color: #000000;
`;

export const MealFoodTableStyled = styled.div`
  max-width: 700px;
  padding: 15px;
  display: grid;
  grid-template-columns: 1fr 5fr 2fr 2fr 2fr 2fr;
  grid-auto-rows: 50px;
`;

export const BreakStyled = styled.hr`
  color: #AAAAAA;
  margin-top: 10px;
`;

export default MealStyled;
