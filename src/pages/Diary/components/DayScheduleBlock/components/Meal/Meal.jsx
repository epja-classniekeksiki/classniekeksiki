import { observer } from 'mobx-react';
import React from 'react';
import AddIcon from '../../../../../../shared/icons/AddIcon.jsx';
import ChevronDownIcon from '../../../../../../shared/icons/ChevronDownIcon.jsx';
import ChevronUpIcon from '../../../../../../shared/icons/ChevronUpIcon.jsx';
import CloseIcon from '../../../../../../shared/icons/CloseIcon.jsx';
import { ButtonWrapperStyled } from '../../../../Diary.styled.js';
import store from '../../../../store/store.js';
import MealStyled, {
  BreakStyled,
  MealFoodTableStyled, MealHeaderStyled, MealNameStyled, MealTitleStyled,
} from './Meal.styled';
import AddFoodWindow from '../AddFoodWindow';

const Meal = observer(({
  name,
  products,
  children,
}) => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const [active, setActive] = React.useState(false);
  const alias = localStorage.getItem('username');

  return (
    <MealStyled>
      <MealHeaderStyled>
        <MealTitleStyled>
          {children}
          <MealNameStyled>{name}</MealNameStyled>
        </MealTitleStyled>
        <ButtonWrapperStyled onClick={() => setIsExpanded((prev) => !prev)}>
          {isExpanded
            ? <ChevronUpIcon /> : <ChevronDownIcon />}
        </ButtonWrapperStyled>
      </MealHeaderStyled>

      {isExpanded && (
      <>
        <BreakStyled />
        { products?.length > 0
        && (
        <MealFoodTableStyled>
          <div> </div>
          <div> </div>
          <p>F</p>
          <p>P</p>
          <p>C</p>
          <b>Cal</b>
          {products && products?.map((product) => (
            <>
              {/* eslint-disable-next-line max-len */}
              <ButtonWrapperStyled onClick={() => store.deleteFood(name.toLowerCase(), product.id, alias)}>
                <CloseIcon />
              </ButtonWrapperStyled>
              <p>{product.name}</p>
              <p>{parseInt(product.fat, 10).toFixed(2)}</p>
              <p>{parseInt(product.protein, 10).toFixed(2)}</p>
              <p>{parseInt(product.carbohydrates, 10).toFixed(2)}</p>
              <b>{parseInt(product.calories, 10).toFixed(2)}</b>
            </>
          ))}
        </MealFoodTableStyled>
        )}
        <div style={{
          width: '100%',
          display: 'flex',
          justifyContent: 'center',
        }}
        >
          <ButtonWrapperStyled onClick={() => setActive(!active)}>
            <AddIcon />
          </ButtonWrapperStyled>
        </div>
      </>
      )}
      {/* eslint-disable-next-line max-len */}
      <AddFoodWindow active={active} setActive={() => setActive(!active)} mealName={name.toLowerCase()} />

    </MealStyled>
  );
});

export default Meal;
