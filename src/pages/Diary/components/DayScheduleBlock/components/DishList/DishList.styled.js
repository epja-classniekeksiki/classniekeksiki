import styled from 'styled-components';

export const DishesContainer = styled.div`
  width: 700px;
  display:flex;
  flex-direction:column;
  gap:8px;
  height:265px;
  overflow:scroll;
  overflow-x: hidden;
`;
