import React from 'react';
import { DishesContainer } from './DishList.styled';
import Dish from './Dish/Dish.jsx';

import store from '../../../../store/store';

function DishList({ dishList, mealName }) {
  return (
    <DishesContainer>
      {dishList.map((dish) => {
        const dishData = {
          productList: [
            {
              id: (Math.floor(Math.random() * 1000000000)),
              name: dish.recipe.label,
              carbohydrates: Math.floor(dish.recipe.totalNutrients.CHOCDF.quantity),
              fat: Math.floor(dish.recipe.totalNutrients.FAT.quantity),
              protein: Math.floor(dish.recipe.totalNutrients.PROCNT.quantity),
              calories: Math.floor(dish.recipe.totalNutrients.ENERC_KCAL.quantity),
            },
          ],
          alias: localStorage.getItem('username'),
        };

        function addFood() {
          store.addFoodProduct(localStorage.getItem('username'), mealName, dishData);
        }

        return <Dish dishData={dishData.productList[0]} onClick={addFood} />;
      })}

    </DishesContainer>
  );
}

export default DishList;
