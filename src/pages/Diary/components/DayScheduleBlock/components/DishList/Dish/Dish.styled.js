import styled from 'styled-components';

export const DishItem = styled.div`
  display:flex;
  justify-content:space-between;
  cursor:pointer;
  width:650px;
  box-shadow: 0px 4px 4px 0 rgba(0, 0, 0, 0.25);
  padding:0 20px;
  transition:all 0.3s;
  &:hover{
    background-color:#FFBB33;
    transition:all 0.3s;
  }
`;

export const DishName = styled.div`
  width: 320px;
  height:60px;
  white-space: pre-wrap;
  display:flex;
  align-items:center;
`;

export const DishNutrition = styled.div`
  width:175px;
  display:flex;
  justify-content:space-between;
  padding-right:5px;
`;

export const Calories = styled.div`
  width:90px;
  font-family: 'Roboto',serif;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  display:flex;
  align-items:center;
`;
export const Macronutrients = styled.ul`
  display:inline-flex;
  justify-content:center;
  align-items:flex-start;
  flex-direction:column;
  list-style-type: none;
`;

export const MacronutrientsItem = styled.li`
  font-family: 'Roboto',serif;
  font-style: normal;
  font-weight: 100;
  font-size: 12px;
`;
