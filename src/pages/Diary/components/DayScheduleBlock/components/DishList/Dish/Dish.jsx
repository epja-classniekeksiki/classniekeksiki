import React from 'react';
import {
  DishItem,
  DishName,
  DishNutrition,
  Calories,
  Macronutrients,
  MacronutrientsItem,
} from './Dish.styled';

function Dish({ dishData, onClick }) {
  return (
    <DishItem onClick={onClick}>
      <DishName>{dishData.name}</DishName>
      <DishNutrition>
        <Calories>
          {dishData.calories}
          kkal
        </Calories>

        <Macronutrients>
          <MacronutrientsItem>
            P:
            {' '}
            {`  ${dishData.protein}`}
          </MacronutrientsItem>
          <MacronutrientsItem>
            F:
            {' '}
            {`  ${dishData.fat}`}
          </MacronutrientsItem>
          <MacronutrientsItem>
            C:
            {' '}
            {`  ${dishData.carbohydrates}`}
          </MacronutrientsItem>
        </Macronutrients>
      </DishNutrition>
    </DishItem>
  );
}

export default Dish;
