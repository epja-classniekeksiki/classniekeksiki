import { observer } from 'mobx-react';
import React from 'react';
import { H2, H3 } from '../../../../shared/typography';
import DiarySummaryBlockStyled, {
  CalorieStyled,
  DailyProgressStyled, MacroStyled,
} from './DiarySummaryBlock.styled';
import store from '../../store/store';

const DiarySummaryBlock = observer(() => (
  <DiarySummaryBlockStyled>
    <H2>Total: </H2>
    <div>
      <MacroStyled>
        Fats:
        {' '}
        {store.totalFats.toFixed(2)}
        g
      </MacroStyled>
      <MacroStyled>
        Carbohydrates:
        {' '}
        {store.totalCarbs.toFixed(2)}
        g
      </MacroStyled>
      <MacroStyled>
        Proteins:
        {' '}
        {store.totalProteins.toFixed(2)}
        g
      </MacroStyled>
      <CalorieStyled>
        Calories:
        {' '}
        {store.totalCalories.toFixed(2)}
      </CalorieStyled>
    </div>

    <div>
      <H3>Daily Progress</H3>
      <DailyProgressStyled
        max={store.dialyCalorieIntake}
        value={store.totalCalories}
        text={`${store.totalCalories.toFixed(2)}/${store.dialyCalorieIntake} `}
      />
    </div>
  </DiarySummaryBlockStyled>
));

export default DiarySummaryBlock;
