import styled from 'styled-components';

const DiarySummaryBlockStyled = styled.div`
  background-color: #FFBB33;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 15px;
  padding: 21px;
  display: flex;
  flex-direction: column;
  row-gap: 15px;
  align-self: start;
`;

export const MacroStyled = styled.p`
  padding-left: 5px;
  font-size: 24px;
  font-weight: 300;
`;

export const CalorieStyled = styled(MacroStyled)`
  font-weight: 400;
`;

export const DailyProgressStyled = styled.progress`
  height: 42px;
  width: 350px;
  text-align:center;
  position: relative;
  border-radius: 15px;

  &::-webkit-progress-value {
    background-color: #3377FF;
    border-radius: 7px;
  }

  &[value]::-webkit-progress-bar {
    background-color: #fff;
    border-radius: 7px;
  }

  &:before {
    content: "${(props) => props.text}";
    position: absolute;
    top: 25%;
    left: ${(props) => 50 - (props.text.length)}%;
  }
`;

export default DiarySummaryBlockStyled;
