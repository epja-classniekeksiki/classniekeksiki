import renderer from 'react-test-renderer';
import React from 'react';
import DiarySummaryBlock from '../DiarySummaryBlock';

describe('DiarySummaryBlock component', () => {
  describe('DiarySummaryBlock render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<DiarySummaryBlock />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
