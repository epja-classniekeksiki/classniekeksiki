import { makeAutoObservable } from 'mobx';
import {
  deleteFood, fetchMeals, addFood,
} from '../../../api';

class DiaryStore {
  dialyCalorieIntake = 2000;
  activeMeal = undefined;
  mealsfetchingStatus = 'notFetched';
  breakfast = [];
  lunch = [];
  dinner = [];
  snack = [];

  constructor() {
    makeAutoObservable(this);
  }

  async addFoodProduct(alias, meal, dish) {
    try {
      await addFood(alias, meal, dish);
    } catch (e) {
      console.log(e);
    }
    this[meal].push(dish.productList[0]);
  }

  async fetchMeals() {
    this.mealsfetchingStatus = 'fetching';
    try {
      const alias = localStorage.getItem('username');
      const breakfast = await fetchMeals(alias, 'breakfast');
      this.breakfast = breakfast ? breakfast.productList : [];
      const lunch = await fetchMeals(alias, 'lunch');
      this.lunch = lunch ? lunch.productList : [];
      const dinner = await fetchMeals(alias, 'dinner');
      this.dinner = dinner ? dinner.productList : [];
      const snack = await fetchMeals(alias, 'snack');
      this.snack = snack ? snack.productList : [];
      this.mealsfetchingStatus = 'fetched';
    } catch {
      this.mealsfetchingStatus = 'failedFetching';
    }
  }

  async deleteFood(meal, id, alias) {
    try {
      await deleteFood(meal, id, alias);
      this[meal] = this[meal].filter((food) => food.id !== id);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }

  get totalFats() {
    let res = 0;
    res += this.breakfast
      .concat(this.lunch)
      .concat(this.dinner)
      .concat(this.snack)
      .reduce((acc, el) => acc + parseInt(el.fat, 10), 0);
    return res;
  }
  get totalCarbs() {
    let res = 0;
    res += this.breakfast
      .concat(this.lunch)
      .concat(this.dinner)
      .concat(this.snack)
      .reduce((acc, el) => acc + parseInt(el.carbohydrates, 10), 0);
    return res;
  }

  get totalProteins() {
    let res = 0;
    res += this.breakfast
      .concat(this.lunch)
      .concat(this.dinner)
      .concat(this.snack)
      .reduce((acc, el) => acc + parseInt(el.protein, 10), 0);
    return res;
  }

  get totalCalories() {
    let res = 0;
    res += this.breakfast
      .concat(this.lunch)
      .concat(this.dinner)
      .concat(this.snack)
      .reduce((acc, el) => acc + parseInt(el.calories, 10), 0);
    return res;
  }
}

export default new DiaryStore();
