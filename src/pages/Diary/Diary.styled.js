import styled from 'styled-components';

const DiaryWrapperStyled = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const DiaryLayoutStyled = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  column-gap: 20px;
`;

export const ButtonWrapperStyled = styled.span`
  cursor: pointer;
`;

export default DiaryWrapperStyled;
