import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './RecipePreviewWindow.module.css';

function RecipePreviewWindow({
  name,
  backgroundImage,
  ingredientsCount,
  calories,
  cookingTime,
  id,
}) {
  const navigate = useNavigate();

  function openRecipePage() {
    navigate(`../recipe/${id}`, { replace: true });
  }

  return (
    <div
      className={styles.item_wrapper}
      style={{ backgroundImage: `url(${backgroundImage})` }}
      onClick={openRecipePage}
    >
      <div className={styles.info_wrapper}>
        <h3 className={styles.recipe_name}>
          {' '}
          {name}
          {' '}
        </h3>

        <div className={styles.recipe_detailed_info}>
          <p className={styles.ingredients_count}>
            {ingredientsCount}
            {' '}
            ingredients
          </p>
          <p className={styles.time_calories_count}>
            {cookingTime}
            min |
            {calories}
            kKal
          </p>
        </div>
      </div>
    </div>
  );
}

export default RecipePreviewWindow;
