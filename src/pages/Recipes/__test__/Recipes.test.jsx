import renderer from 'react-test-renderer';
import React from 'react';
import Recipes from '../Recipes';

describe('Recipes component', () => {
  describe('Recipes render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<Recipes />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
