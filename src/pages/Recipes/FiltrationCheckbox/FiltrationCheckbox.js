import React from 'react';

import styles from './FiltrationCheckbox.module.css';

function FiltrationCheckbox(props) {
  return (
    <label className={styles.checkbox_body}>
      <input type="checkbox" name={props.name} className={styles.checkbox} onChange={props.onChange} />
      <div className={styles.filtration_switch}>
        {props.displaying_name}
      </div>
    </label>
  );
}

export default FiltrationCheckbox;
