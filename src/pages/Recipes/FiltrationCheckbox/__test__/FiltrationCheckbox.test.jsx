import renderer from 'react-test-renderer';
import React from 'react';
import FiltrationCheckbox from '../FiltrationCheckbox';

describe('FiltrationCheckbox component', () => {
  describe('FiltrationCheckbox render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<FiltrationCheckbox />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
