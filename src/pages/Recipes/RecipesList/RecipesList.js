import React from 'react';

import styles from './RecipesList.module.css';
import RecipePreviewWindow from '../RecipePreviewWindow';

function RecipesList({ recipesList }) {
  return (
    <div className={styles.recipe_grid}>
      {recipesList.map((item, index) => {
        const id = item.recipe.uri.split('#')[1];
        return (
          <RecipePreviewWindow
            name={item.recipe.label}
            key={`item_${index}`}
            ingredientsCount={item.recipe.ingredients.length}
            cookingTime={item.recipe.totalTime}
            calories={Math.trunc(item.recipe.calories)}
            backgroundImage={item.recipe.images.REGULAR.url}
            id={id}
          />
        );
      })}
    </div>
  );
}

export default RecipesList;
