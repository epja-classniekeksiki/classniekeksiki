import React, { useState } from 'react';

import styles from './Recipes.module.css';

import RecipesList from './RecipesList';

import SearchPanel from './SearchPanel';

function Recipes(props) {
  const [recipesList, setRecipesList] = useState([]);

  return (
    <section className={styles.recipes_panel}>
      <div className={styles.headline}>
        <h2>All recipes</h2>
      </div>

      <SearchPanel onSubmit={setRecipesList} />

      {(recipesList.length === 0) ? false : <RecipesList recipesList={recipesList} />}

    </section>
  );
}

export default Recipes;
