import renderer from 'react-test-renderer';
import React from 'react';
import SearchPanel from '../SearchPanel';

describe('SearchPanel component', () => {
  describe('SearchPanel render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<SearchPanel />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
