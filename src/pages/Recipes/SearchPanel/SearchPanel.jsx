import React, { useState, useRef } from 'react';

import FiltrationWindow from '../FiltrationWindow';
import searchIcon from '../../../icons/search-icon.png';
import filtrationIcon from '../../../icons/filtration_icon.jpg';

import { getRecipesList } from '../../../api/edamam';

import styles from './SearchPanel.module.css';

function SearchPanel(props) {
  const [filtrationVisible, setFiltrationVisible] = useState(false);

  const searchBar = useRef();

  const filtrationFlags = useRef({
    diet_pref: new Set(),
    meal_pref: new Set(),
    health_pref: new Set(),
  });

  function handleEnterPush(event) {
    if (event.key === 'Enter') {
      makeSearchRequest();
    }
  }

  function handleFiltrationClick() {
    setFiltrationVisible(!filtrationVisible);
  }

  function handleDietPreferencesChange(event) {
    const pushedSwitchName = event.target.name;
    if (event.target.checked) {
      filtrationFlags.current.diet_pref.add(pushedSwitchName);
      return;
    }
    filtrationFlags.current.diet_pref.delete(pushedSwitchName);
  }

  function handleHealthPreferencesChange(event) {
    const pushedSwitchName = event.target.name;
    if (event.target.checked) {
      filtrationFlags.current.health_pref.add(pushedSwitchName);
      return;
    }
    filtrationFlags.current.health_pref.delete(pushedSwitchName);
  }

  function handleMealPreferencesChange(event) {
    const pushedSwitchName = event.target.name;
    if (event.target.checked) {
      filtrationFlags.current.meal_pref.add(pushedSwitchName);
      return;
    }
    filtrationFlags.current.meal_pref.delete(pushedSwitchName);
  }

  async function makeSearchRequest() {
    const dietConf = [...filtrationFlags.current.diet_pref]
      .map((flag) => `&diet=${flag}`)
      .join('');
    const healthConf = [...filtrationFlags.current.health_pref]
      .map((flag) => `&health=${flag}`)
      .join('');
    const mealConf = [...filtrationFlags.current.meal_pref]
      .map((flag) => `&mealType=${flag}`)
      .join('');

    const recipes = await getRecipesList(
      searchBar.current.value,
      dietConf,
      healthConf,
      mealConf,
    );
    props.onSubmit(recipes);
  }

  return (
    <div className={styles.search_panel}>
      <button className={styles.filtration_btn} onClick={handleFiltrationClick}>
        <span>Filtration</span>
        <img src={filtrationIcon} alt="" />
      </button>

      <input
        type="text"
        placeholder="Search..."
        id={styles.search_bar}
        onKeyDown={handleEnterPush}
        ref={searchBar}
      />

      <button type="submit" id={styles.search_btn} onClick={makeSearchRequest}>
        <img src={searchIcon} alt="" width="30px" height="30px" />
      </button>
      <FiltrationWindow
        isVisible={filtrationVisible}
        closeWindow={handleFiltrationClick}
        healthHandler={handleHealthPreferencesChange}
        dietHandler={handleDietPreferencesChange}
        mealHandler={handleMealPreferencesChange}
      />
    </div>
  );
}

export default SearchPanel;
