import renderer from 'react-test-renderer';
import React from 'react';
import FiltrationWindow from '../FiltrationWindow';

describe('FiltrationWindow component', () => {
  describe('FiltrationWindow render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<FiltrationWindow />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
