import React from 'react';

import styles from './FiltrationWindow.module.css';

import closeBtnImg from '../../../icons/close_icon.png';
import FiltrationCheckbox from '../FiltrationCheckbox';

function FiltrationWindow({
  isVisible, closeWindow, dietHandler, mealHandler, healthHandler,
}) {
  const visibility = isVisible ? { opacity: 1 } : { opacity: 0, pointerEvents: 'none' };

  return (
    <div className={styles.filtration_window} style={visibility}>
      <div className="controls">
        <button type="button" className={styles.close_btn}><img src={closeBtnImg} alt="" onClick={closeWindow} /></button>
      </div>

      <div className={styles.pref_selector}>
        <h4>
          Diet labels
        </h4>
        <div className={styles.checkbox_wrapper}>
          <FiltrationCheckbox name="balanced" displaying_name="Balanced" onChange={dietHandler} />
          <FiltrationCheckbox name="high-protein" displaying_name="High-Protein" onChange={dietHandler} />
          <FiltrationCheckbox name="low-fat" displaying_name="Low-Fat" onChange={dietHandler} />
        </div>
      </div>
      <br />

      <div className={styles.pref_selector}>
        <h4>
          Meal types
        </h4>
        <div className={styles.checkbox_wrapper}>
          <FiltrationCheckbox name="breakfast" displaying_name="Breakfast" onChange={mealHandler} />
          <FiltrationCheckbox name="lunch" displaying_name="Lunch" onChange={mealHandler} />
          <FiltrationCheckbox name="dinner" displaying_name="Dinner" onChange={mealHandler} />
          <FiltrationCheckbox name="snack" displaying_name="Snack" onChange={mealHandler} />
          <FiltrationCheckbox name="teatime" displaying_name="Teatime" onChange={mealHandler} />
        </div>
      </div>

      <br />
      <div className={styles.pref_selector}>
        <h4>
          Health labels
        </h4>
        <div className={styles.checkbox_wrapper}>
          <FiltrationCheckbox name="gluten-free" displaying_name="Gluten-Free" onChange={healthHandler} />
          <FiltrationCheckbox name="vegetarian" displaying_name="Vegetarian" onChange={healthHandler} />
          <FiltrationCheckbox name="low-sugar" displaying_name="Low Sugar" onChange={healthHandler} />
          <FiltrationCheckbox name="dairy-free" displaying_name="Dairy-Free" onChange={healthHandler} />
        </div>
      </div>
    </div>
  );
}

export default FiltrationWindow;
