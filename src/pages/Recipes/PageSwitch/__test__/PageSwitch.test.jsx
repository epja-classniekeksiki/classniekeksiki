import renderer from 'react-test-renderer';
import React from 'react';
import PageSwitch from '../PageSwitch';

describe('PageSwitch component', () => {
  describe('PageSwitch render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<PageSwitch />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
