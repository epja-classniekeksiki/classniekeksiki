import React from 'react';

import styles from './PageSwitchButton.module.css';

function PageSwitchButton({ onClick, displayedNumber }) {
  return (
    <button type="button" className={styles.switch_btn} onClick={onClick}>
      {displayedNumber}
      {' '}
    </button>
  );
}

export default PageSwitchButton;
