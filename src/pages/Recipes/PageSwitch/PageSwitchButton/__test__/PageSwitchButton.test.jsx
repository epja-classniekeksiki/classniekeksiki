import renderer from 'react-test-renderer';
import React from 'react';
import PageSwitchButton from '../PageSwitchButton';

describe('PageSwitchButton component', () => {
  describe('PageSwitchButton render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<PageSwitchButton />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
