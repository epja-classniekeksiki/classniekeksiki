import React from 'react';

import styles from './PageSwitch.module.css';

import PageSwitchButton from './PageSwitchButton';

function PageSwitch(props) {
  const buttonsToRender = [];
  const { pagesLinks } = props;
  const { curPage } = props;

  if (curPage === 1) {
    buttonsToRender.push(curPage);
    for (let i = curPage + 1; i < curPage + 3; i++) {
      if (pagesLinks.has(i)) {
        buttonsToRender.push(i);
      }
    }
  }
  return (

    <div className={styles.page_navigation_menu}>
      {buttonsToRender.map((buttonNumber) => <PageSwitchButton displayedNumber={buttonNumber} onClick={props.pageBtnClicked} />)}
      {buttonsToRender.length === 3 ? <span>...</span> : false}
    </div>
  );
}

export default PageSwitch;
