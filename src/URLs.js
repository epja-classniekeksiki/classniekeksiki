import { getNavigations, getNavigationsValue } from '@ijl/cli';
import pkg from '../package.json';

const baseUrl = getNavigationsValue(`${pkg.name}.main`);
const navs = getNavigations();

export const URLs = {
  baseUrl,
  diary: {
    url: navs[`link.${pkg.name}.diary`],
  },
  recipes: {
    url: navs[`link.${pkg.name}.recipes`],
  },
  login: {
    url: navs[`link.${pkg.name}.login`],
  },
  profile: {
    url: navs[`link.${pkg.name}.profile`],
  },
  recipe: {
    url: navs[`link.${pkg.name}.recipe`],
  },

};
