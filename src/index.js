import React from 'react';
import ReactDom from 'react-dom';
import App from './App.jsx';

export default function () {
  return <App />;
}

export const mount = (Component) => {
  ReactDom.render(<Component />, document.getElementById('app'));

  if (module.hot) {
    module.hot.accept('./App.jsx', () => {
      ReactDom.render(<App />, document.getElementById('app'));
    });
  }
};

export const unmount = () => {
  ReactDom.unmountComponentAtNode(document.getElementById('app'));
};
