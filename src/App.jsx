import React, { useEffect, useState } from 'react';
import {
  BrowserRouter, Navigate, Route, Routes,
} from 'react-router-dom';
import { URLs } from './URLs';
import styles from './App.module.css';
import Header from './shared/Header';
import Recipes from './pages/Recipes';
import Diary from './pages/Diary';
import LayoutStyled from './shared/styled/Layout.styled';
import Login from './pages/Login';
import Profile from './pages/Profile';
import { AuthContext } from './context';
import RecipeInfo from './pages/RecipeInfo/RecipeInfo.jsx';

function App() {
  const [username, setUsername] = useState(null);
  useEffect(() => {
    setUsername(localStorage.getItem('username'));
  }, []);
  return (
    <div className={styles.app_wrapper}>
      <AuthContext.Provider value={{
        username,
        setUsername,
      }}
      >

        <BrowserRouter basename={URLs.baseUrl}>
          <Header />
          <LayoutStyled>
            <Routes>
              {username
                ? (
                  <>
                    <Route
                      path="/"
                      element={<Navigate replace to={URLs.diary.url} />}
                    />
                    <Route path={URLs.diary.url} element={<Diary />} />
                    <Route path={`${URLs.profile.url}/*`} element={<Profile />} />
                  </>
                )
                : (
                  <>
                    <Route
                      path="/"
                      element={<Navigate replace to={URLs.recipes.url} />}
                    />
                    <Route path={URLs.login.url} element={<Login />} />
                  </>
                )}
              <Route path={URLs.recipe.url} element={<RecipeInfo />} />
              <Route path={URLs.recipes.url} element={<Recipes />} />

            </Routes>
          </LayoutStyled>
        </BrowserRouter>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
