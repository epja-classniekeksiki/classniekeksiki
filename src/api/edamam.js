const APIkey = '23d0b898ba9de2d82036352d7d926b83';
const AppID = '65dec05c';

export const getRecipesList = async function (query, dietConf, healthConf, mealConf) {
  const API = `https://api.edamam.com/api/recipes/v2?type=public&app_id=${AppID}&app_key=${APIkey}`;
  const request = `${API}&q=${query}${dietConf}${mealConf}${healthConf}`;
  let response = await fetch(request);
  response = await response.json();
  const recipes = response.hits;

  return recipes;
};

export const getRecipeByID = async function (id) {
  const request = `https://api.edamam.com/api/recipes/v2/${id}?type=public&app_id=${AppID}&app_key=${APIkey}`;
  let response = await fetch(request);
  response = await response.json();
  const { recipe } = response;

  return recipe;
};


export const getImageByURL = async function (url) {
  
  let response 
  try{
    response = await fetch(url);
  }
  catch(e){

    console.log(e)
    return 
  }
  
  response = await response.blob();

  return URL.createObjectURL(response)
};