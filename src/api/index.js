import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const API = getConfigValue('classniekeksiki.api');

export const fetchMeals = async (alias, meal) => {
  const res = await axios.get(`${API}/${meal}/${alias}`);
  return res.data;
};

export const deleteFood = async (meal, id, alias) => {
  try {
    const response = await axios.delete(`${API}/${meal}/${id}/${alias}`);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
};

export const addFood = async (alias, meal, foodData) => {
  try {
    const response = await axios.post(`${API}/${meal}`, foodData);
    console.log(response);
  } catch (e) {
    console.error(e);
  }
};
export const getUserData = async (alias) => {
  const res = await axios.get(`${API}/user/${alias}`);
  return res.data;
};
