import React from 'react';

export default function DinnerIcon() {
  return (
    <img alt="Dinner" src="https://img.icons8.com/external-vitaliy-gorbachev-lineal-vitaly-gorbachev/60/000000/external-turkey-christmas-vitaliy-gorbachev-lineal-vitaly-gorbachev.png" />
  );
}
