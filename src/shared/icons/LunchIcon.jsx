import React from 'react';

export default function LunchIcon() {
  return (
    <img alt="Lunch" src="https://img.icons8.com/external-justicon-lineal-justicon/64/000000/external-dinner-valentines-day-justicon-lineal-justicon.png" />
  );
}
