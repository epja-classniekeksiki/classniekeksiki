import React from 'react';

export default function BreakfastIcon() {
  return (
    <img alt="breakfast" src="https://img.icons8.com/external-icongeek26-outline-icongeek26/64/000000/external-breakfast-healthy-lifestyle-icongeek26-outline-icongeek26.png" />
  );
}
