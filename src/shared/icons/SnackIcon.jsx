import React from 'react';

export default function SnackIcon() {
  return (
    <img alt="Snack" src="https://img.icons8.com/external-vitaliy-gorbachev-lineal-vitaly-gorbachev/60/000000/external-lunch-box-back-to-school-vitaliy-gorbachev-lineal-vitaly-gorbachev.png" />
  );
}
