import renderer from 'react-test-renderer';
import React from 'react';
import ModalWindow from '../ModalWindow';

describe('ModalWindow component', () => {
  describe('ModalWindow render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<ModalWindow />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
