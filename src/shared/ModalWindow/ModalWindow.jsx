import React from 'react';
import classNames from 'classnames';
import styles from './ModalWindow.module.css';

function ModalWindow({ active, setActive, children }) {
  return (
    <div className={active ? classNames(styles.modal, styles.active) : styles.modal} onClick={() => setActive()}>
      <div className={active ? classNames(styles.content, styles.active) : styles.content} onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
}

export default ModalWindow;
