import React from 'react';
import styles from './SuccessWindow.module.css';
import ModalWindow from '../ModalWindow';

function SuccessWindow({ message, setMessage }) {
  return (
    <ModalWindow active={message} setActive={() => { setMessage(''); }}>
      Success:
      {' '}
      {message}
      <button type="button" className={styles.close_btn} onClick={() => { setMessage(''); }}>
        <span>&times;</span>
      </button>
    </ModalWindow>
  );
}

export default SuccessWindow;
