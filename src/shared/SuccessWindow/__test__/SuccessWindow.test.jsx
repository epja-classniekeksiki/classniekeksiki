import renderer from 'react-test-renderer';
import React from 'react';
import SuccessWindow from '../SuccessWindow';

describe('SuccessWindow component', () => {
  describe('SuccessWindow render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<SuccessWindow />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
