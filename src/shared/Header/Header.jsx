import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import styles from './Header.module.css';

import cakeLogo from '../../icons/cake-logo-transparent.png';
import stubProfilePicture from '../../icons/stub_profile_picture.png';
import { URLs } from '../../URLs';
import { AuthContext } from '../../context';

function Header() {
  const { username } = useContext(AuthContext);
  return (
    <header id="header">
      <nav>
        <div className="logo_wrapper">
          <img src={cakeLogo} alt="" height={50} width={50} />
        </div>
        <div className={styles.nav_menu_wrapper}>
          <ul className={styles.nav_menu}>
            {username
              ? <li className={styles.nav_menu_item}><Link to={URLs.diary.url}>Main</Link></li>
              : null}
            <li className={styles.nav_menu_item}><Link to={URLs.recipes.url}>Recipes</Link></li>
          </ul>
        </div>
      </nav>

      <div className={styles.profile_panel}>
        {username
          ? (
            <Link to={URLs.profile.url}>
              <div className={styles.username}>{username.slice(0, 2).toUpperCase()}</div>
            </Link>
          )
          : (
            <Link to={URLs.login.url}>
              <img src={stubProfilePicture} alt="profile" height={50} width={50} />
            </Link>
          )}

      </div>

    </header>
  );
}

export default Header;
