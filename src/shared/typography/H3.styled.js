import styled from 'styled-components';

const H3Styled = styled.h3`
  font-size: 24px;
  font-weight: 400;
`;

export default H3Styled;
