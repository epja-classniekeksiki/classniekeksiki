import styled from 'styled-components';

const H2Styled = styled.h2`
  font-size: 36px;
  font-weight: 700;
`;

export default H2Styled;
