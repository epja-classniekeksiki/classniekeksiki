import styled from 'styled-components';

const H1Styled = styled.h1`
  font-size: 48px;
  font-weight: 700;
  text-align: center;
  margin-top: 15px;
`;

export default H1Styled;
