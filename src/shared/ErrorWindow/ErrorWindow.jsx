import React from 'react';
import styles from './ErrorWindow.module.css';
import ModalWindow from '../ModalWindow';

function ErrorWindow({ errMessage, setErrMessage }) {
  return (
    <ModalWindow active={errMessage} setActive={() => { setErrMessage(''); }}>
      Error:
      {' '}
      {errMessage}
      <button type="button" className={styles.close_btn} onClick={() => { setErrMessage(''); }}>
        <span>&times;</span>
      </button>
    </ModalWindow>
  );
}

export default ErrorWindow;
