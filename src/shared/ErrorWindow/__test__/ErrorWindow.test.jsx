import renderer from 'react-test-renderer';
import React from 'react';
import ErrorWindow from '../ErrorWindow';

describe('ErrorWindow component', () => {
  describe('ErrorWindow render', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(<ErrorWindow />);
      const tree = component.toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});
