import { navigations, config } from '../ijl.config';

module.exports = {
  getConfigValue: (conf) => config[conf],
  getNavigations: (_) => navigations,
  getNavigationsValue: (val) => navigations[val],
};
