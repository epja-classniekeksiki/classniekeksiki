const pkg = require('./package.json');

module.exports = {
  apiPath: 'stubs/api',
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
  },
  navigations: {
    'classniekeksiki.main': '/classniekeksiki',
    'link.classniekeksiki.diary': '/diary',
    'link.classniekeksiki.recipes': '/recipes',
    'link.classniekeksiki.login': '/login',
    'link.classniekeksiki.profile': '/profile',
    'link.classniekeksiki.recipe': '/recipe/:id'
  },
  features: {
    classniekeksiki: {
      // add your features here in the format [featureName]: { value: string }
    },
  },
  config: {
    'classniekeksiki.api': 'https://slimsecret.herokuapp.com',
    'classniekeksiki.edamam': 'https://api.edamam.com/api/recipes/v2?type=public&app_id=65dec05c&app_key=23d0b898ba9de2d82036352d7d926b83',
  },
};
